# Introduction



<aside>
    <strong>Base URL</strong>: <code>http://0.0.0.0:8000</code>
</aside>

This documentation aims to provide all the information you need to work with our API.

<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>

Full-text search implemented using the library Laravel Scout + Meilisearch.

After importing existing records into your search indexes, you can view them by following the link http://0.0.0.0:7700/

Indexes are created automatically.

<b>
Attention!</b><br>
When changing data in the database directly, the indexes are NOT UPDATE! In order for the indexes to be updated, you need to add, edit or delete records via the API.





# TODO List API


An API that will allow you to manage a list of tasks


## Installation
1. Create .env file and copy into it content from .env.example

2. Deploy docker container 

```bash
  docker-compose up -d --build
```

3. Install dependencies


```bash
  docker-compose exec app composer install
```

4. Run serve

```bash
  docker-compose exec -d app php artisan serve --host=0.0.0.0 --port=8000
```
5. Run migrations
```bash
  docker-compose exec app php artisan migrate
```
6. Fill database via seeds

```bash
  docker-compose exec app php artisan db:seed
```
7. Import all of your existing records into your search indexes

```bash
  docker-compose exec app php artisan scout:import App\\Models\\Task
```

8. Update sortable and filtered indexes for mielsearch

```bash
  docker-compose exec app php artisan app:update-tasks-attribute-command
```

## Information

Full-text search implemented using the library Laravel Scout + Meilisearch. 

After importing existing records into your search indexes, you can view them by following the link http://0.0.0.0:7700/

Indexes are created automatically.

**Attention!**  
When changing data in the database directly, the indexes are NOT UPDATE!
In order for the indexes to be updated, you need to add, edit or delete records via the API.


## Documentation

[Documentation](http://0.0.0.0:8000/docs)

## Testing

The repository has a file TodoList.postman_collection.json
You can import it into Postman for testing. 

You can use credentials testuser@gmail.com / password for login or any other email from database and 'password' for login.


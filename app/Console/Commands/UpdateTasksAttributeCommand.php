<?php

namespace App\Console\Commands;

use App\Models\Task;
use Illuminate\Console\Command;
use Meilisearch\Client;


class UpdateTasksAttributeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-tasks-attribute-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle():void
    {
        $client = new Client(config('scout.meilisearch.url'));
        $client->index('tasks')->updateSortableAttributes(Task::SORTABLE);
        $client->index('tasks')->updateFilterableAttributes(Task::FILTERED);
    }
}

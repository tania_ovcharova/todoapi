<?php

namespace App\DTO;

use App\Enums\TaskStatus;
use Carbon\Carbon;
use Illuminate\Validation\Rules\Enum;
use Spatie\DataTransferObject\DataTransferObject;

class FilterTaskDTO extends DataTransferObject
{
    public readonly ?string $status;
    public readonly ?int $priority;
    public readonly string $search;
    public readonly array $sort;
    public readonly int $user_id;

    public function __construct($userId, ?string $status, ?int $priority, string $search="", array $sort=[]){
        $this->search = $search;
        $this->status = $status;
        $this->priority = $priority;
        $this->sort = $sort;
        $this->user_id = $userId;
    }
}

<?php

namespace App\Http\Controllers;

use App\DTO\FilterTaskDTO;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\TaskFilterRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Resources\TaskResourse;
use App\Models\Task;
use App\Services\TaskService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;


/**
 * @group Tasks
 * @authenticated
 */
class TaskController extends Controller
{

    private TaskService $taskService;


    public function __construct(TaskService $taskService)
    {

        $this->taskService = $taskService;
    }


    /**
     * List of tasks
     * @response 200 {"data":[{"task_id":1,"title":"Sapiente deserunt quas repellendus enim repellat exercitationem explicabo aut.","description":"Molestias sapiente voluptas vero consequatur reiciendis harum ut officia. Ipsa corrupti nulla consectetur est. Quasi est ab molestiae aut. Omnis quibusdam odit ut et eius asperiores possimus.","created_at":"2023-12-16T13:56:43.000000Z","completed_at":null,"status":"todo","priority":"4"},{"task_id":2,"title":"Natus nesciunt rerum quaerat possimus doloremque maiores.","description":"Suscipit ea ea qui ducimus laboriosam facilis fugiat. Fugit perspiciatis sapiente doloribus possimus sit est. Quasi animi eum necessitatibus.","created_at":"2023-12-14T13:56:43.000000Z","completed_at":null,"status":"todo","priority":"3"}]}
     */
    public function index(TaskFilterRequest $request)
    {
        $tasks = $this->taskService->getAllItemsByFilters(new FilterTaskDTO(
            auth()->user()->id,
            $request->input('status', null),
            $request->input('priority', null),
            $request->input('search', ''),
            $request->input('sort', [])
        ));
        return TaskResourse::collection($tasks);
    }


    /**
     * Create task
     *
     * @response 200 {"success":true,"task":{"task_id":313,"title":"new title34","description":"new description34","created_at":"2023-12-17T15:10:58.000000Z","completed_at":null,"status":"todo","priority":"2","parent_id":null}}
     * @response 422 {"message":"The title field is required.","errors":{"title":["The title field is required."]}}
     */
    public function store(CreateTaskRequest $request)
    {
        return $this->taskService->create($request->validated());
    }


    /**
     * Update task
     * @response 200 {"success":true,"task":{"task_id":308,"title":"new title","description":"new description2","created_at":"2023-12-17T14:42:36.000000Z","completed_at":null,"status":"todo","priority":"2"}}
     * @response 403 {"success":false,"message":"You do not own this task."}
     * @response 422 {"message":"The selected priority is invalid.","errors":{"priority":["The selected priority is invalid."]}}
     */
    public function update(Task $task, UpdateTaskRequest $request)
    {
        $this->authorize('update', $task);

        $result = $this->taskService->update($task, $request->validated());

        return response()->json($result);
    }

    /**
     * Complete task
     *
     * You can not to mark as completed a task that has uncompleted tasks
     *
     * @param Task $task
     * @return JsonResponse
     * @throws AuthorizationException
     *
     * @response 200 {"data":{"task_id":309,"title":"new title2","description":"new description2","created_at":"2023-12-17T14:43:58.000000Z","completed_at":null,"status":"done","priority":"2"}}
     * @response 403 {"success":false,"message":"You do not own this task."}
     * @response 422 {"success":false,"message":"It is not possible to mark as completed a task that has uncompleted tasks"}
     */
    public function complete(Task $task)
    {
        $this->authorize('update', $task);

        if ($this->taskService->checkTaskHasUnCompletedChildren($task)) {
            return response()->json(['success' => false, 'message' => 'It is not possible to mark as completed a task that has uncompleted tasks'], 422);
        }

        $result = $this->taskService->complete($task);

        return response()->json($result);
    }


    /**
     * Delete task
     * @param Task $task
     * @return JsonResponse
     * @throws AuthorizationException
     *
     * @response 200 {"success":true,"message":"Record successfully deleted"}
     * @response 403 {"success":false,"message":"You do not own this task."}
     * @response 422 {"success":false,"message":"Unable to delete completed task"}
     *
     */
    public function destroy(Task $task)
    {
        $this->authorize('delete', $task);

        if ($this->taskService->checkIfTheTaskIsCompleted($task)) {
            return response()->json(['success' => false, 'message' => 'Unable to delete completed task'], 422);
        }

        $result = $this->taskService->delete($task);

        return response()->json($result);
    }

}

<?php

namespace App\Http\Requests;

use App\Enums\TaskStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class TaskFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'status' => ['nullable', new Enum(TaskStatus::class)],
            'priority' => ['nullable', 'in:1,2,3,4,5'],
            'search' => 'string|nullable',
            'sort' => 'nullable|array|in:"priority:asc","priority:desc","created_at:asc","created_at:desc","completed_at:asc","completed_at:desc"'
        ];
    }
}

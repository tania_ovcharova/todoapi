<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Task extends Model
{
    use HasFactory, Searchable;

    protected $guarded = [];

    protected $casts = [
        'completed_at' => 'datetime',
    ];

    public const SORTABLE = ['id', 'created_at', 'completed_at', 'priority'];

    public const FILTERED = ['status', 'priority', 'user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }


    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'status' => $this->status,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'completed_at' => $this->completed_at,
            'priority' => $this->priority,

        ];
    }




}

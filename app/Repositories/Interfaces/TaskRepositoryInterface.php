<?php

namespace App\Repositories\Interfaces;

use App\DTO\FilterTaskDTO;
use App\Models\Task;

interface TaskRepositoryInterface
{
    public function getAllItemsByFilters(FilterTaskDTO $data);

    public function create(array $data);

    public function update(Task $task, array $data);

    public function delete(Task $task);
}

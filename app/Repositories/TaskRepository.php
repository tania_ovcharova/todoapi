<?php

namespace App\Repositories;

use App\DTO\FilterTaskDTO;
use App\Models\Task;
use App\Repositories\Interfaces\TaskRepositoryInterface;
use Meilisearch\Endpoints\Indexes;

class TaskRepository implements TaskRepositoryInterface
{
    public function getAllItemsByFilters(FilterTaskDTO $data)
    {
        $tasksQuery = Task::search($data->search,
            function(Indexes $meilisearch, string $query, array $options) use ($data){
                $options['sort'] = $data->sort;
                $options = $this->castFilter($options, $data, 'userId');
                $options = $this->castFilter($options, $data, 'status');
                $options = $this->castFilter($options, $data, 'priority');
                info($options);
                return $meilisearch->search($query, $options);
            });
        return $tasksQuery->get();
    }

    public function castFilter(array $options, FilterTaskDTO $data, string $filterName):array{
        if(!empty($data->{$filterName})) {
            $options['filter'][] = $filterName.' = ' . $data->{$filterName};
        }
        return $options;
    }


    public function create(array $data):Task
    {
        return Task::create($data);
    }

    public function update(Task $task, array $data):void
    {
        $task->update($data);
    }

    public function delete(Task $task):void
    {
        $task->delete();
    }
}

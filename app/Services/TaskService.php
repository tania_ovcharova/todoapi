<?php

namespace App\Services;

use App\DTO\FilterTaskDTO;
use App\Http\Resources\TaskResourse;
use App\Models\Task;
use App\Repositories\Interfaces\TaskRepositoryInterface;
use App\Repositories\TaskRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Meilisearch\Endpoints\Indexes;

class TaskService
{

    private TaskRepository $taskRepository;


    public function __construct(TaskRepositoryInterface $taskRepository){

        $this->taskRepository = $taskRepository;
    }


    public function getAllItemsByFilters(FilterTaskDTO $data): Collection
    {
        return $this->taskRepository->getAllItemsByFilters($data);
    }

    public function update(Task $task, array $data): array
    {
        try {
            $this->taskRepository->update($task, $data);
            $task->fresh();
            return [ 'success' => true, 'task' => new TaskResourse($task)];
        } catch (\Exception $ex){
            return ['success' => false, 'message' => 'Failed to update record'];
        }
    }

    public function complete(Task $task):array
    {
        try {
            $this->taskRepository->update($task, ['status' => 'done', 'completed_at' =>Carbon::now()]);
            $task->fresh();
            return ['success' => true, 'task' => $task];
        } catch (\Exception $ex){
            return ['success' => false, 'message' => 'Failed to complete record'];
        }
    }

    public function create(mixed $data):array
    {
        try {
            $task = $this->taskRepository->create($data);
            return ['success' => true, 'task' =>  new TaskResourse($task)];
        } catch (\Exception $ex){
            return ['success' => false, 'message' => 'Failed to create record'];
        }
    }

    public function delete(Task $task):array
    {
        try {
            $this->taskRepository->delete($task);
            return ['success' => true, 'message' => 'Record successfully deleted'];
        } catch (\Exception $ex){
            return ['success' => false, 'message' => 'Failed to delete record'];
        }
    }


    public function checkTaskHasUncompletedChildren(Task $task):bool
    {
        return $this->hasUncompletedChildrenRecursive($task);
    }


    /**
     *
     * A recursive function to check the 'todo' status of child tasks.
     *
     * @param  Task  $parentTask
     * @return bool
     */
    protected function hasUncompletedChildrenRecursive(Task $parentTask): bool
    {
        $children = $parentTask->children;
        foreach ($children as $child) {
            // Checking whether the status of the current child task is 'todo'
            if ($child->status === 'todo') {
                return true;
            }

            // recursively checking the child tasks of the current task
            if ($this->hasUncompletedChildrenRecursive($child)) {
                return true;
            }
        }
        // If none of the child tasks has the status 'todo', return false
        return false;
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function checkIfTheTaskIsCompleted(Task $task):bool
    {
        return $task->status === 'done';
    }

}

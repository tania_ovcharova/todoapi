<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();

        Task::truncate();
        User::truncate();

        Schema::enableForeignKeyConstraints();
        $myUser = User::factory()->create([
            'email' => 'testuser@gmail.com',
        ]);
        $tasks = Task::factory(10)->create([
            'user_id' => $myUser->id,
        ]);
        $deep = 3;
        $this->fillTasksTable($myUser->id, $tasks, $deep);

        $users = User::factory(3)->create();
        foreach ($users as $user){
            //$task without parent
            $tasks = Task::factory(10)->create([
                'user_id' => $user->id,
            ]);
            $this->fillTasksTable($user->id, $tasks, $deep);
        }

    }

    private function fillTasksTable(int $userId, mixed $tasks, int $deep)
    {
        if($deep > 0) {
            foreach ($tasks as $task) {
                $childrenCount = random_int(0, 3);
                $newTasks = Task::factory($childrenCount)->create([
                    'user_id' => $userId,
                    'parent_id' => $task->id
                ]);
                $this->fillTasksTable($userId, $newTasks, $deep - 1);
            }
        }
    }
}
